class AddPromoCodeAndReferalCodeAndBonusPercentageToUsers < ActiveRecord::Migration
  def change
    add_column :users, :promo_code, :string, index: true
    add_column :users, :referral_code, :string, index: true
    add_column :users, :bonus_percentage, :float, default: 10.0
  end
end
