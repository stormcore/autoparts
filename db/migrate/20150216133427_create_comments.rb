class CreateComments < ActiveRecord::Migration
  def change
    create_table :comments do |t|
      t.references :user, index: true
      t.references :order, index: true
      t.integer :parent_comment_id
      t.text :content

      t.timestamps
    end
  end
end
