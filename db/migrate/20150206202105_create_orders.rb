class CreateOrders < ActiveRecord::Migration
  def change
    create_table :orders do |t|
      t.references :user, index: true
      t.integer :year
      t.string :engine_volume
      t.string :vin
      t.string :details

      t.timestamps
    end
  end
end
