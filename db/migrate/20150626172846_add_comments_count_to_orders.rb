class AddCommentsCountToOrders < ActiveRecord::Migration

  def self.up

    add_column :orders, :comments_count, :integer, :null => false, :default => 0

  end

  def self.down

    remove_column :orders, :comments_count

  end

end
