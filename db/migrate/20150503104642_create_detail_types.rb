class CreateDetailTypes < ActiveRecord::Migration
  def change
    create_table :detail_types do |t|
      t.string :name

      t.timestamps null: false
    end
    add_reference :orders, :detail_type
  end
end
