class AddFilterOptionsToUsers < ActiveRecord::Migration
  def change
    add_column :users, :filter_options, :hstore
  end
end
