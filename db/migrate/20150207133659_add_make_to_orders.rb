class AddMakeToOrders < ActiveRecord::Migration
  def change
    add_reference :orders, :make
  end
end
