class CreateTransmissions < ActiveRecord::Migration
  def change
    create_table :transmissions do |t|
      t.string :name

      t.timestamps null: false
    end
    add_reference :orders, :transmission
  end
end
