class CreateEmailSubscriptions < ActiveRecord::Migration
  def change
    create_table :email_subscriptions do |t|
      t.references :user, index: true
      t.datetime :expires
      t.boolean :active

      t.timestamps null: false
    end
    add_foreign_key :email_subscriptions, :users
  end
end
