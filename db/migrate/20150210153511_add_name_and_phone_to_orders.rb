class AddNameAndPhoneToOrders < ActiveRecord::Migration
  def change
    add_column :orders, :name, :string
    add_column :orders, :phone, :string
  end
end
