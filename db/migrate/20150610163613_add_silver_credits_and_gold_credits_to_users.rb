class AddSilverCreditsAndGoldCreditsToUsers < ActiveRecord::Migration
  def change
    add_column :users, :silver_credits, :float, default: 0
    add_column :users, :gold_credits, :float, default: 0
  end
end
