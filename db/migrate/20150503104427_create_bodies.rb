class CreateBodies < ActiveRecord::Migration
  def change
    create_table :bodies do |t|
      t.string :name

      t.timestamps null: false
    end
    add_reference :orders, :body
  end
end
