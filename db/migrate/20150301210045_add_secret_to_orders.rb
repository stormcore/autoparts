class AddSecretToOrders < ActiveRecord::Migration
  def change
    add_column :orders, :secret, :string
  end
end
