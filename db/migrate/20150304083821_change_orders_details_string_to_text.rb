class ChangeOrdersDetailsStringToText < ActiveRecord::Migration
  def change
    change_column :orders, :details, :text
  end
end
