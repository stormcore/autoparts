class CreatePosts < ActiveRecord::Migration
  def change
    create_table :posts do |t|
      t.text :title
      t.text :keywords
      t.text :short_description
      t.text :full_description
      t.boolean :published

      t.timestamps null: false
    end
  end
end
