# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)
Order.destroy_all
City.destroy_all
cities = City.create([{ name: 'Минск и обл.' }, { name: 'Брест и обл.' }, { name: 'Витебск и обл.' },
                      { name: 'Гомель и обл.' }, { name: 'Гродно и обл.' }, { name: 'Могилёв и обл.' },
                      { name: 'Россия' }, { name: 'Казахстан' }])

Make.destroy_all
makes = Make.create([{ name: 'Не указано' }, { name: 'Acura' }, { name: 'Alfa Romeo' }, { name: 'Asia Motors' },
                     { name: 'Aston Martin' }, { name: 'Audi' }, { name: 'BAW' }, { name: 'Bentley' },
                     { name: 'BMW' }, { name: 'Brilliance' }, { name: 'Bugatti' }, { name: 'Buick' }, { name: 'BYD' },
                     { name: 'Cadillac' }, { name: 'Changan' }, { name: 'Changfeng' }, { name: 'Chery' },
                     { name: 'Chevrolet' }, { name: 'Chrysler' }, { name: 'Citroen' }, { name: 'Dacia' },
                     { name: 'Daewoo' }, { name: 'DAF' }, { name: 'Daihatsu' }, { name: 'Datsun' },
                     { name: 'De Lorean' }, { name: 'Derways' }, { name: 'Dodge' }, { name: 'FAW' },
                     { name: 'Ferrari' }, { name: 'Fiat' }, { name: 'Ford' },
                     { name: 'Foton' }, { name: 'Geely' }, { name: 'Geo' }, { name: 'GMC' }, { name: 'Great Wall' },
                     { name: 'Hafei' }, { name: 'Haima' }, { name: 'Honda' }, { name: 'Hummer' }, { name: 'Hyundai' },
                     { name: 'Infiniti' }, { name: 'Iran Khodro' }, { name: 'Isuzu' }, { name: 'Iveco' }, { name: 'JAC' },
                     { name: 'Jaguar' }, { name: 'Jeep' }, { name: 'JMC' }, { name: 'Kia' }, { name: 'KTM' },
                     { name: 'Lamborghini' }, { name: 'Lancia' }, { name: 'Land Rover' }, { name: 'Lexus' },
                     { name: 'Lifan' }, { name: 'Lincoln' }, { name: 'Lotus' }, { name: 'Man' }, { name: 'Maserati' }, { name: 'Maxus' },
                     { name: 'Maybach' }, { name: 'Mazda' }, { name: 'Mercedes' }, { name: 'Mercury' }, { name: 'MG' },
                     { name: 'MINI' }, { name: 'Mitsubishi' }, { name: 'Nissan' }, { name: 'Oldsmobile' },
                     { name: 'Opel' }, { name: 'Peugeot' }, { name: 'Piaggio' }, { name: 'Plymouth' },
                     { name: 'Pontiac' }, { name: 'Porsche' }, { name: 'Proton' }, { name: 'Renault' },
                     { name: 'Samsung' }, { name: 'Rolls-Royce' }, { name: 'Rover' },
                     { name: 'Saab' }, { name: 'Saturn' }, { name: 'Scion' }, { name: 'Seat' }, { name: 'ShuangHuan' },
                     { name: 'Skoda' }, { name: 'SMA' }, { name: 'Smart' }, { name: 'Ssangyong' }, { name: 'Subaru' },
                     { name: 'Suzuki' }, { name: 'Tata' }, { name: 'Tesla' }, { name: 'Toyota' }, { name: 'Triumph' },
                     { name: 'Vauxhall' }, { name: 'Volkswagen' }, { name: 'Volvo' }, { name: 'Vortex' },
                     { name: 'Yuejin' }, { name: 'ZhongXing' }, { name: 'Zotye' }, { name: 'ВАЗ' }, { name: 'ГАЗ' },
                     { name: 'ЗАЗ' }, { name: 'Иж' }, { name: 'Москвич' }, { name: 'ТагАЗ' }, { name: 'УАЗ' }])

Body.destroy_all
bodies = Body.create([{ name: 'Не указано' }, { name: 'Седан' }, { name: 'Хэтчбэк' }, { name: 'Универсал' }, { name: 'Кроссовер/Внедорожник' },
                   { name: 'Купе' }, { name: 'Кабриолет' }, { name: 'Лимузин' }, { name: 'Минивэн' },
                   { name: 'Пикап' }, { name: 'Родстер' }])

Engine.destroy_all
engines = Engine.create([{ name: 'Не указано' }, { name: 'Бензин' }, { name: 'Дизель' }, { name: 'Газ' }, { name: 'Гибрид' },
                         { name: 'Электро' }])

Transmission.destroy_all
transmissions = Transmission.create([{ name: 'Не указано' }, { name: 'Ручная' }, { name: 'Автомат' },
                                     { name: 'Вариатор' }, { name: 'Типтроник' }, { name: 'Робот' }])

DetailType.destroy_all
detail_types = DetailType.create([{ name: 'Не указано' }, { name: 'Кондиционеры/Климат' }, { name: 'КПП/Трансмиссия' },
                                  { name: 'Тормозная система' }, { name: 'Оптика/Автостекла/Зеркала' },
                                  { name: 'Кузов/Салон' }, { name: 'Шины/Диски' }, { name: 'Двигатель' },
                                  { name: 'Расходные материалы' }, { name: 'Выхлопная система' },
                                  { name: 'Подвеска/Рулевое' }])