require File.expand_path('../boot', __FILE__)

require 'rails/all'

# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(*Rails.groups)

module Autoparts
  class Application < Rails::Application
    # Settings in config/environments/* take precedence over those specified here.
    # Application configuration should go into files in config/initializers
    # -- all .rb files in that directory are automatically loaded.

    # Set Time.zone default to the specified zone and make Active Record auto-convert to this zone.
    # Run "rake -D time" for a list of tasks for finding time zone names. Default is UTC.
    # config.time_zone = 'Central Time (US & Canada)'

    # The default locale is :en and all translations from config/locales/*.rb,yml are auto loaded.
    # config.i18n.load_path += Dir[Rails.root.join('my', 'locales', '*.{rb,yml}').to_s]
    # config.i18n.default_locale = :de
    config.i18n.default_locale = :ru
    config.time_zone = 'Minsk'
    config.active_record.default_timezone = :local
    config.active_job.queue_adapter = :sidekiq

    config.assets.precompile += Ckeditor.assets
    config.assets.precompile += %w( ckeditor/* )
    config.autoload_paths += %W(#{config.root}/app/models/ckeditor)
    config.after_initialize do
      Rails.application.config.assets.precompile = Rails.application.config.assets.precompile - Ckeditor.assets
    end

    uglifier = Uglifier.new output: { comments: :none }

    config.assets.js_compressor = uglifier

    # To keep all comments instead or only keep copyright notices (the default):
    # uglifier = Uglifier.new output: { comments: :all }
    # uglifier = Uglifier.new output: { comments: :copyright }

    config.assets.compile = true
    config.assets.debug = false

    config.middleware.use Rack::Deflater
    config.middleware.insert_before ActionDispatch::Static, Rack::Deflater
    config.cache_store = :memory_store

    config.middleware.use HtmlCompressor::Rack,
                          compress_css: true,
                          compress_javascript: false,
                          css_compressor: Sass,
                          enabled: true,
                          javascript_compressor: uglifier,
                          preserve_line_breaks: false,
                          remove_comments: true,
                          remove_form_attributes: false,
                          remove_http_protocol: false,
                          remove_https_protocol: false,
                          remove_input_attributes: true,
                          remove_intertag_spaces: false,
                          remove_javascript_protocol: true,
                          remove_link_attributes: true,
                          remove_multi_spaces: true,
                          remove_quotes: true,
                          remove_script_attributes: false,
                          remove_style_attributes: true,
                          simple_boolean_attributes: true,
                          simple_doctype: false

end
end
