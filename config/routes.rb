Rails.application.routes.draw do
  mount Ckeditor::Engine => '/ckeditor'
  resources :posts

  devise_for :admin_users, ActiveAdmin::Devise.config
  ActiveAdmin.routes(self)
  resources :detail_types

  resources :engines

  resources :transmissions

  resources :bodies

  resources :makes

  resources :cities

  resources :orders do
    resources :comments
  end

  get 'my-orders/:email', to: 'orders#my', as: :my_orders, constraints: { email: /[^\/]+/ }

  get 'my-orders', to: 'orders#my_search', as: :my_orders_search_form

  get 'add_email_subscription_month', to: 'email_subscriptions#add_email_subscription_month', as: :add_email_subscription_month
  get 'email_subscription_status', to: 'email_subscriptions#email_subscription_status', as: :email_subscription_status

  post 'my-orders', to: 'orders#my', as: :my_orders_search, constraints: { email: /[^\/]+/ }

  post 'orders-filter', to: 'orders#filter', as: :orders_filter_form

  get 'orders-filter', to: 'orders#filter', as: :orders_filter

  get 'orders-unanswered', to: 'orders#unanswered', as: :orders_unanswered

  get 'welcome/index'

  get 'welcome/dealers'

  get 'welcome/add_funds'

  root 'welcome#index'

  devise_for :users, controllers: { registrations: 'registrations' }

  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
  # root 'welcome#index'

  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end
