# == Schema Information
#
# Table name: bodies
#
#  id         :integer          not null, primary key
#  name       :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class Body < ActiveRecord::Base
  has_many :orders
end
