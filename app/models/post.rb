# == Schema Information
#
# Table name: posts
#
#  id                :integer          not null, primary key
#  title             :text
#  keywords          :text
#  short_description :text
#  full_description  :text
#  published         :boolean
#  created_at        :datetime         not null
#  updated_at        :datetime         not null
#

class Post < ActiveRecord::Base
  scope :published, -> { where(published: true) }
  scope :not_published, -> { where(published: false) }

  has_attached_file :picture, :styles => { :medium => "700x700>", :thumb => "300x300>" }, :default_url => "/images/:style/missing.png"
  validates_attachment_content_type :picture, :content_type => /\Aimage\/.*\Z/
end
