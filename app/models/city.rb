# == Schema Information
#
# Table name: cities
#
#  id         :integer          not null, primary key
#  name       :string
#  created_at :datetime
#  updated_at :datetime
#

class City < ActiveRecord::Base
  has_many :orders
  has_many :users
end
