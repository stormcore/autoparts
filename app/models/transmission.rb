# == Schema Information
#
# Table name: transmissions
#
#  id         :integer          not null, primary key
#  name       :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class Transmission < ActiveRecord::Base
  has_many :orders
end
