# == Schema Information
#
# Table name: detail_types
#
#  id         :integer          not null, primary key
#  name       :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class DetailType < ActiveRecord::Base
  has_many :orders
end
