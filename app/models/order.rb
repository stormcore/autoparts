# == Schema Information
#
# Table name: orders
#
#  id              :integer          not null, primary key
#  year            :integer
#  engine_volume   :string
#  vin             :string
#  details         :text
#  created_at      :datetime
#  updated_at      :datetime
#  city_id         :integer
#  make_id         :integer
#  email           :string
#  name            :string
#  phone           :string
#  secret          :string
#  model           :string
#  body_id         :integer
#  transmission_id :integer
#  engine_id       :integer
#  detail_type_id  :integer
#  deleted_at      :datetime
#  comments_count  :integer          default(0), not null
#

class Order < ActiveRecord::Base
  acts_as_paranoid

  belongs_to :user
  belongs_to :city
  belongs_to :make
  belongs_to :body
  belongs_to :detail_type
  belongs_to :engine
  belongs_to :transmission
  has_many :comments, dependent: :destroy
  validates :city, :year, :make, :detail_type, :model, :phone, :email, :details, :name, presence: true
  validates :details, length: { maximum: 1500 }
  validates :engine_volume, length: { maximum: 30 }
  validates :name, length: { maximum: 30 }
  validates :vin, length: { maximum: 30 }
  validates :phone, length: { maximum: 30 }
  validates :model, length: { maximum: 30 }
  validates :year, length: { maximum: 4 }
  validates :email, length: { maximum: 255 }
  validates_email_format_of :email, message: 'Пожалуйста, введите ваш настоящий e-mail адрес для получения уведомлений об ответах на ваш заказ (вы сможете отменить подписку в любой момент)'
  validate :make_name_valid
  validate :detail_type_name_valid

  after_commit :notify_email_subscribers, :on => :create

  scope :uncommented, -> { where(comments_count: 0) }

  def notify_email_subscribers
    subscriptions = EmailSubscription.where('expires > ?', DateTime.now).joins(:user)
                    .where("users.filter_options -> 'makes' like '%#{make_id}%'")
                    .where("users.filter_options -> 'bodies' like '%#{body_id}%'")
                    .where("users.filter_options -> 'cities' like '%#{city_id}%'")
                    .where("users.filter_options -> 'engines' like '%#{engine_id}%'")
                    .where("users.filter_options -> 'detail_types' like '%#{detail_type_id}%'")
                    .where("users.filter_options -> 'transmissions' like '%#{transmission_id}%'")

    subscriptions.each do |subscription|
      EmailSubscriptionsMailer.email_subscription_mail(self, subscription.user).deliver_later(wait: 7.seconds)
    end
  end

  private

  def make_name_valid
    errors.add('Марка', 'не может быть пустой') unless make.name != 'Не указано'
  end

  def detail_type_name_valid
    errors.add('Тип детали', 'не может быть пустым') unless detail_type.name != 'Не указано'
  end
end
