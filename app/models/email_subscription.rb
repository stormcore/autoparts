# == Schema Information
#
# Table name: email_subscriptions
#
#  id         :integer          not null, primary key
#  user_id    :integer
#  expires    :datetime
#  active     :boolean
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class EmailSubscription < ActiveRecord::Base
  belongs_to :user

  validates :user_id, uniqueness: true

  def add_subscribed_months(months_count)
    if user.spend_credits(months_count * subscription_month_price)
      if expires < DateTime.now
        self.expires = DateTime.now + months_count.month
      else
        self.expires = expires + months_count.month
      end
      self.active = true
      self.save
    else false
    end
  end

  def subscription_month_price
    1000
  end
end
