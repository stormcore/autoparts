# == Schema Information
#
# Table name: users
#
#  id                     :integer          not null, primary key
#  email                  :string           default(""), not null
#  encrypted_password     :string           default(""), not null
#  reset_password_token   :string
#  reset_password_sent_at :datetime
#  remember_created_at    :datetime
#  sign_in_count          :integer          default(0), not null
#  current_sign_in_at     :datetime
#  last_sign_in_at        :datetime
#  current_sign_in_ip     :string
#  last_sign_in_ip        :string
#  created_at             :datetime
#  updated_at             :datetime
#  name                   :string
#  phone                  :string
#  information            :string
#  city_id                :integer
#  filter_options         :hstore
#  approved               :boolean          default(FALSE), not null
#  deleted_at             :datetime
#  promo_code             :string
#  referral_code          :string
#  bonus_percentage       :float            default(10.0)
#  silver_credits         :float            default(0.0)
#  gold_credits           :float            default(0.0)
#

class User < ActiveRecord::Base
  acts_as_paranoid

  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  belongs_to :city

  has_many :comments
  has_one :email_subscription, dependent: :destroy

  validates :name, :phone, presence: true

  # serialize :filter_options
  store_accessor :filter_options, :makes, :cities, :bodies, :detail_types, :engines, :transmissions

  before_create :generate_referral_code
  after_create :send_admin_mail

  def active_for_authentication?
    super && approved?
  end

  def inactive_message
    if !approved?
      :not_approved
    else
      super # Use whatever other message
    end
  end

  def add_silver_credits(amount)
    self.silver_credits += amount
  end

  def spend_credits(amount)
    if self.silver_credits < amount
      if gold_credits < amount
        false
      else
        self.gold_credits -= amount
        pay_referral_bonus(amount)
        save
      end
    else
      self.silver_credits -= amount
      save
    end
  end

  def add_gold_credits(amount)
    self.silver_credits += amount
    save
  end

  def set_approved
    self.approved = true
    add_registration_silver_credits_reward
    set_defaults
    create_email_subscription
  end

  def update_filter_options(options)
    new_options = Hash.new
    new_options['makes'] = options['makes'].to_s.gsub!(/\"\"/, '').gsub!(/\"/, '').gsub!(', ]', ']').gsub!('[', '').gsub!(']', '').split(', ').map {|n| n.to_i}
    new_options['cities'] = options['cities'].to_s.gsub!(/\"\"/, '').gsub!(/\"/, '').gsub!(', ]', ']').gsub!('[', '').gsub!(']', '').split(', ').map {|n| n.to_i}
    new_options['bodies'] = options['bodies'].to_s.gsub!(/\"\"/, '').gsub!(/\"/, '').gsub!(', ]', ']').gsub!('[', '').gsub!(']', '').split(', ').map {|n| n.to_i}
    new_options['engines'] = options['engines'].to_s.gsub!(/\"\"/, '').gsub!(/\"/, '').gsub!(', ]', ']').gsub!('[', '').gsub!(']', '').split(', ').map {|n| n.to_i}
    new_options['detail_types'] = options['detail_types'].to_s.gsub!(/\"\"/, '').gsub!(/\"/, '').gsub!(', ]', ']').gsub!('[', '').gsub!(']', '').split(', ').map {|n| n.to_i}
    new_options['transmissions'] = options['transmissions'].to_s.gsub!(/\"\"/, '').gsub!(/\"/, '').gsub!(', ]', ']').gsub!('[', '').gsub!(']', '').split(', ').map {|n| n.to_i}

    self.filter_options = new_options
    self.save
  end

  def set_default_filters
    self.filter_options = { 'makes' => Make.pluck(:id), 'cities' => City.pluck(:id), 'bodies' => Body.pluck(:id),
                            'engines' => Engine.pluck(:id), 'detail_types' => DetailType.pluck(:id),
                            'transmissions' => Transmission.pluck(:id) }
    self.save
  end

  private
  def send_admin_mail
    AdminUser.all.each do |admin_user|
      #AdminMailer.new_user_waiting_for_approval(self, admin_user).deliver_later(wait: 5.seconds)
    end
  end

  def generate_referral_code
    random_code = SecureRandom.hex(3) + DateTime.now.to_i.to_s
    self.referral_code = random_code
  end

  def add_registration_silver_credits_reward
    referrer = set_referrer_user
    if referrer.nil?
      add_silver_credits(1000)
    else
      referrer.add_silver_credits(1500)
      referrer.save
      add_silver_credits(1500)
    end
  end

  def pay_referral_bonus(from_amount)
    referrer = set_referrer_user
    if referrer
      referrer.add_silver_credits(from_amount * (referrer.bonus_percentage / 100.0))
      referrer.save
    end
  end

  def set_referrer_user
    referrer = nil
    unless promo_code.empty?
      referrer = User.find_by(referral_code: promo_code)
    else
      return nil
    end

    referrer unless referrer.nil?
  end

  def set_defaults
    self.bonus_percentage = 10
    set_default_filters
    self.save
  end

  private
  def create_email_subscription
    email_subscription = EmailSubscription.create(user: self, active: false, expires: DateTime.now)
    email_subscription.save
  end

end
