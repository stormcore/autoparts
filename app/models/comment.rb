# == Schema Information
#
# Table name: comments
#
#  id                :integer          not null, primary key
#  user_id           :integer
#  order_id          :integer
#  parent_comment_id :integer
#  content           :text
#  created_at        :datetime
#  updated_at        :datetime
#

class Comment < ActiveRecord::Base
  belongs_to :user, -> { with_deleted }
  belongs_to :order
  counter_culture :order
  belongs_to :parent, class_name: 'Comment', foreign_key: 'parent_comment_id'
  has_many :child_comments, class_name: 'Comment', foreign_key: 'parent_comment_id', dependent: :destroy

  validates :order_id, :content, presence: true

  validates :content, length: { maximum: 5500 }

  scope :top_level, -> { where(parent_comment_id: nil) }
end
