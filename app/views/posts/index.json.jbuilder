json.array!(@posts) do |post|
  json.extract! post, :id, :title, :keywords, :short_description, :full_description, :published
  json.url post_url(post, format: :json)
end
