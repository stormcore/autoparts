json.array!(@detail_types) do |detail_type|
  json.extract! detail_type, :id, :name
  json.url detail_type_url(detail_type, format: :json)
end
