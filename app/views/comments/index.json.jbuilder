json.array!(@comments) do |comment|
  json.extract! comment, :id, :user_id, :order_id, :parent_comment_id, :content
  json.url comment_url(comment, format: :json)
end
