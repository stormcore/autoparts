json.array!(@orders) do |order|
  json.extract! order, :id, :detail_type, :name, :city, :make, :model, :year, :body, :engine, :engine_volume, :transmission,
                :vin, :details
  json.url order_url(order, format: :html)
end
