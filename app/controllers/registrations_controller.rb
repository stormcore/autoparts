class RegistrationsController < Devise::RegistrationsController
  private

  def sign_up_params
    params.require(:user).permit(:name, :phone, :information, :email, :password, :password_confirmation, :promo_code)
  end

  def account_update_params
    params.require(:user).permit(:name, :phone, :city_id, :information, :email, :password, :password_confirmation,
                                 :approved, :current_password)
  end
end
