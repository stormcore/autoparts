class EmailSubscriptionsController < InheritedResources::Base
  before_action :authenticate_user!

  def add_email_subscription_month
    if current_user.email_subscription.add_subscribed_months(1)
      redirect_to email_subscription_status_path, notice: 'Подписка успешно продлена'
    else
      redirect_to email_subscription_status_path, alert: 'Недостаточно средств для продления подписки'
    end
  end

  def email_subscrpition_status
    @email_subscription = current_user.email_subscription.all
    respond_with(@email_subscription)
  end
end
