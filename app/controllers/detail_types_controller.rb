class DetailTypesController < ApplicationController
  before_action :deny_access
  before_action :set_detail_type, only: [:show, :edit, :update, :destroy]

  # GET /detail_types
  # GET /detail_types.json
  def index
    @detail_types = DetailType.all
  end

  # GET /detail_types/1
  # GET /detail_types/1.json
  def show
  end

  # GET /detail_types/new
  def new
    @detail_type = DetailType.new
  end

  # GET /detail_types/1/edit
  def edit
  end

  # POST /detail_types
  # POST /detail_types.json
  def create
    @detail_type = DetailType.new(detail_type_params)

    respond_to do |format|
      if @detail_type.save
        format.html { redirect_to @detail_type, notice: 'Detail type was successfully created.' }
        format.json { render :show, status: :created, location: @detail_type }
      else
        format.html { render :new }
        format.json { render json: @detail_type.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /detail_types/1
  # PATCH/PUT /detail_types/1.json
  def update
    respond_to do |format|
      if @detail_type.update(detail_type_params)
        format.html { redirect_to @detail_type, notice: 'Detail type was successfully updated.' }
        format.json { render :show, status: :ok, location: @detail_type }
      else
        format.html { render :edit }
        format.json { render json: @detail_type.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /detail_types/1
  # DELETE /detail_types/1.json
  def destroy
    @detail_type.destroy
    respond_to do |format|
      format.html { redirect_to detail_types_url, notice: 'Detail type was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_detail_type
    @detail_type = DetailType.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def detail_type_params
    params.require(:detail_type).permit(:name)
  end
end
