class OrdersController < ApplicationController
  before_action :set_order, only: [:show, :edit, :update, :destroy]
  before_action :validate_secret, only: [:edit, :update, :destroy]
  before_action :authenticate_user!, only: [:index, :filter]

  respond_to :html, :json

  def index
    filter
  end

  def filter
    if request.post?
      current_user.update_filter_options(params[:filter_options])
    end

    @orders = Order.where(city: JSON.parse(current_user.filter_options['cities']))
                  .where(make: JSON.parse(current_user.filter_options['makes']))
                  .where(body: JSON.parse(current_user.filter_options['bodies']))
                  .where(engine: JSON.parse(current_user.filter_options['engines']))
                  .where(transmission: JSON.parse(current_user.filter_options['transmissions']))
                  .where(detail_type: JSON.parse(current_user.filter_options['detail_types']))
                  .includes(:city, :make, :detail_type, :body, :engine, :transmission)
                  .order(created_at: :desc)
                  .paginate(page: params[:page], per_page: 15)

    @makes = Make.all
    @cities = City.all
    @bodies = Body.all
    @engines = Engine.all
    @transmissions = Transmission.all
    @detail_types = DetailType.all

    render :index
  end

  def unanswered
    @orders = Order.where(comments_count: 0).order(created_at: :desc).paginate(page: params[:page], per_page: 15)
  end

  def my
    if request.post?
      if params[:email] == ''
        redirect_to root_path, notice: 'Введите Email'
      else
        respond_to do |format|
          @orders = Order.where(email: params[:email]).includes(:city, :make, :detail_type, :body, :engine, :transmission).order(created_at: :desc)

          format.html {redirect_to my_orders_path(email: params[:email])}
          format.json

        end
      end
    else
      @orders = Order.where(email: params[:email]).includes(:city, :make, :detail_type, :body, :engine, :transmission).order(created_at: :desc).paginate(page: params[:page],
                                                                                     per_page: 10)
    end
  end

  def my_search
    render :my_form
  end

  def show
    if validate_secret
      render :show
    else
      if user_signed_in?
        render :view
      else
        redirect_to root_path, notice: 'Отказано в доступе, пожалуйста, авторизуйтесь'
      end
    end
  end

  def new
    @order = Order.new
    respond_with(@order)
  end

  def edit
    if validate_secret
      respond_with(@order)
    else
      redirect_to root_path, notice: 'Отказано в доступе, пожалуйста, авторизуйтесь'
    end
  end

  def create
    unless verify_recaptcha
      redirect_to new_order_path, alert: 'Подтвердите, что вы не спам-робот'
      return
    end
    @order = Order.new(order_params)
    @order.secret = SecureRandom.urlsafe_base64

    cookies.permanent[:email] = @order.email
    cookies.permanent[:name] = @order.name
    cookies.permanent[:phone] = @order.phone
    cookies.permanent[:model] = @order.model
    cookies.permanent[:vin] = @order.vin
    cookies.permanent[:engine_volume] = @order.engine_volume
    cookies.permanent[:year] = @order.year
    cookies.permanent[:city] = @order.city.id
    cookies.permanent[:make] = @order.make.id
    cookies.permanent[:body] = @order.body.id
    cookies.permanent[:engine] = @order.engine.id
    cookies.permanent[:transmission] = @order.transmission.id

    if @order.save
      OrdersMailer.confirmation_mail(@order).deliver_later(wait: 5.seconds)
      redirect_to order_path(@order, secret: @order.secret),
                  notice: 'Ваш заказ успешно создан, секретная ссылка для доступа отправлена на ваш Email. Если письмо не отображается в папке "Входящие", возможно оно находится в папке "Спам"'
    else
      respond_with(@order)
    end
  end

  def update
    if validate_secret
      if @order.update(order_params)
        redirect_to order_path(@order, secret: @order.secret)
      else
        respond_with(@order)
      end
    else
      redirect_to root_path, notice: 'Отказано в доступе, пожалуйста, авторизуйтесь'
    end
  end

  def destroy
    if validate_secret
      email = @order.email
      @order.destroy
      # respond_with(@order)
      redirect_to action: :my, email: email
    else
      redirect_to root_path, notice: 'Отказано в доступе, пожалуйста, авторизуйтесь'
    end
  end

  private

  def set_order
    @order = Order.find(params[:id])
  end

  def order_params
    params.require(:order).permit(:city_id, :make_id, :year, :engine_volume, :vin, :details, :email, :model, :secret,
                                  :name, :phone, :body_id, :detail_type_id, :engine_id, :transmission_id,
                                  :filter_options)
  end

  def validate_secret
    if params[:secret] == @order.secret || params[:order] && params[:order][:secret] == @order.secret
      true
    elsif current_user_admin?
      true
    else
      false
    end
  end
end
