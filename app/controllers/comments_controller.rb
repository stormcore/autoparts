class CommentsController < ApplicationController
  before_action :set_comment, only: [:show, :edit, :update, :destroy]
  before_action :authenticate_user!, only: [:edit, :update, :destroy]

  respond_to :html

  def index
    if validate_secret
      @comments = order.comments.top_level
      respond_with(@comments, @secret)
    else
      if user_signed_in?
        @comments = order.comments.top_level.where(user_id: current_user.id)
        respond_with(@comments)
      else
        redirect_to root_path, notice: 'Отказано в доступе, пожалуйста, авторизуйтесь'
      end
    end
  end

  def show
    @order = order
    if validate_secret
      @secret
      respond_with(@comment, @secret)
    elsif user_signed_in? && @comment.user.id == current_user.id
      respond_with(@comment)
    else
      redirect_to root_path, notice: 'Отказано в доступе, пожалуйста, авторизуйтесь'
    end
  end

  def new
    @comment = order.comments.new
    @comment.parent_comment_id = params[:parent_comment_id]
    if validate_secret
      @secret = order.secret
      respond_with(@comment, @secret)
    elsif !@comment.parent || @comment.parent && @comment.parent[:user_id] == current_user.id
      respond_with(@comment)
    else
      redirect_to root_path, notice: 'Отказано в доступе, пожалуйста, авторизуйтесь'
    end
  end

  def edit
    @order = order
  end

  def create
    @secret_valid = validate_secret
    @comment = Comment.new(comment_params.except(:secret))
    @comment.user = current_user if user_signed_in?
    if @comment.save
      if @comment.user
        if @comment[:parent_comment_id]
          CommentsMailer.new_proposal_mail(@comment.parent).deliver_later(wait: 5.seconds)
        else
          CommentsMailer.new_proposal_mail(@comment).deliver_later(wait: 5.seconds)
        end
      else
        CommentsMailer.new_client_answer_mail(@comment).deliver_later(wait: 5.seconds)
      end
    end
    if @comment[:parent_comment_id]
      if @secret_valid
        redirect_to order_comment_path(order, @comment.parent, secret: order.secret), notice: 'Комментарий успешно создан'
      else
        respond_with(order, @comment.parent)
      end
    else
      respond_with(order, @comment)
    end
  end

  def update
    if @comment.user_id == current_user.id
      @comment.update(comment_params)
      respond_with(order, @comment)
    else
      redirect_to root_path, notice: 'Отказано в доступе, пожалуйста, авторизуйтесь'
    end
  end

  def destroy
    if @comment.user_id == current_user.id
      @comment.destroy
      respond_with(order, @comment)
    else
      redirect_to root_path, notice: 'Отказано в доступе, пожалуйста, авторизуйтесь'
    end
  end

  private

  def set_comment
    @comment = Comment.find(params[:id])
  end

  def comment_params
    params.require(:comment).permit(:user_id, :order_id, :parent_comment_id, :secret, :content)
  end

  def order
    @order = Order.find(params[:order_id])
  end

  def validate_secret
    if params[:secret] || params[:comment] && params[:comment][:secret]
      if params[:secret] == order.secret || params[:comment][:secret] == order.secret
        @secret = order.secret
        true
      else
        false
      end
    else
      false
    end
  end
end
