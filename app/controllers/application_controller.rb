class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :null_session

  helper_method :current_user_admin?

  protected
  def deny_access
    redirect_to root_path, notice: 'Отказано в доступе, пожалуйста, авторизуйтесь'
  end

  def current_user_admin?
    if user_signed_in? && AdminUser.where(email: current_user.email).exists?
      true
    else
      false
    end
  end

  def validate_admin_privileges!
    unless current_user_admin?
      deny_access
    end
  end
end
