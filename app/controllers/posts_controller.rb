class PostsController < InheritedResources::Base
  before_action :validate_admin_privileges!, except: [:show, :index]

  def index
    if current_user_admin?
      @posts = Post.all.order(created_at: :desc)
                   .paginate(page: params[:page], per_page: 5)
    else
      @posts = Post.published.order(created_at: :desc)
                   .paginate(page: params[:page], per_page: 5)
    end
  end

  private

    def post_params
      params.require(:post).permit(:title, :keywords, :short_description, :full_description, :published, :picture)
    end
end

