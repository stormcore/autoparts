function toggle(filterName) {
    checkboxes = document.getElementsByName(filterName);
    for(var i=0, n=checkboxes.length;i<n;i++) {
        checkboxes[i].checked = true;
    }
};

function unToggle(filterName) {
    checkboxes = document.getElementsByName(filterName);
    for(var i=0, n=checkboxes.length;i<n;i++) {
        checkboxes[i].checked = false;
    }
};

var updateInterval = null;

function bindStopTimer() {
    $('#stop-timer, .ui-accordion-header').click(function(){
        clearInterval(updateInterval);
        $('#stop-timer').removeClass('btn-primary').addClass('hidden');
        $('#timer-text').text('Автообновление приостановлено до ручного обновления страницы').addClass('text-danger animated flash infinite');
        $('#refresh-button').removeClass('hidden').addClass('animated fadeIn');
    });
}

$(document).on('ready page:load', function() {
    bindStopTimer();
    clearInterval(updateInterval);
    var REFRESH_INTERVAL_IN_MILLIS = 45000;
    if ($('#orders').length > 0) {
        updateInterval = setInterval(function(){
            //disable page scrolling to top after loading page content
            Turbolinks.enableTransitionCache(true);

        //pass current page url to visit method
            if ($('#orders').length > 0) {
                clearInterval(updateInterval);
                Turbolinks.visit(location.toString());
            }

        //enable page scroll reset in case user clicks other link
            Turbolinks.enableTransitionCache(false);
        }, REFRESH_INTERVAL_IN_MILLIS);
    }
});
