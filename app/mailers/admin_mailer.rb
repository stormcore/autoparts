class AdminMailer < ApplicationMailer
  default from: 'info@zappo.by'
  def new_user_waiting_for_approval(user, admin_user)
    mail(to: admin_user.email, subject: "[zappo-admin] Поступила заявка на регистрацию пользователя: #{user.name} - #{user.email}")
  end
end
