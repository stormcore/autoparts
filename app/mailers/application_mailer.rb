class ApplicationMailer < ActionMailer::Base
  default from: 'info@zappo.by'
  layout 'mailer'
end
