class EmailSubscriptionsMailer < ApplicationMailer
  def email_subscription_mail(order, user)
    @order = order
    mail(to: user.email, subject: "#{user.name}, поступил новый заказ, который вам интересен!")
  end
end
