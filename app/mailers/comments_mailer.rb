class CommentsMailer < ApplicationMailer
  default from: 'info@zappo.by'

  def new_proposal_mail(comment)
    @comment = comment
    mail(to: @comment.order.email, subject: "#{@comment.order.name}, Вам поступило сообщение от продавца")
  end

  def new_client_answer_mail(comment)
    @comment = comment
    mail(to: @comment.parent.user.email, subject: "Вам поступил ответ от покупателя: #{@comment.order.name}")
  end
end
