class OrdersMailer < ActionMailer::Base
  default from: 'info@zappo.by'
  layout 'mailer'


  def confirmation_mail(order)
    @order = order
    mail(to: @order.email, subject: "#{@order.name}, Ваш заказ успешно создан")
  end
end
