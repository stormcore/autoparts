ActiveAdmin.register User do
  controller do
    def scoped_collection
      super.with_deleted
    end
  end

  index do
    id_column
    column :approved
    column :email
    column :name
    column :phone
    column :silver_credits
    column :gold_credits
    column :created_at
    column :city_id
    column :referral_code
    column :promo_code
    column :bonus_precentage
    column :deleted_at
    column :sign_in_count
    column :last_sign_in_at
    actions
  end

  action_item :view, only: :show do
    # link_to 'Подтвердить пользователя', approve_user_path(user), method: 'put'
    link_to 'Подтвердить пользователя', approve_user_admin_user_path(user), method: 'put'
  end

  member_action :approve_user, method: :put do
    if resource.approved == false
      resource.set_approved
      resource.save
      redirect_to resource_path, notice: 'Пользователь успешно подтвержден'
    else
      redirect_to resource_path, alert: 'Пользователь уже подтвержден'
    end
  end

  permit_params :name, :phone, :city_id, :information, :email, :password, :password_confirmation,
                :approved, :current_password, :bonus_precentage, :approved,
                :silver_credits, :gold_credits, :promo_code, :referral_code
end
