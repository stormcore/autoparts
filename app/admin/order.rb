ActiveAdmin.register Order do
  controller do
    def scoped_collection
      super.with_deleted
    end
  end
end
