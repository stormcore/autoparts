ActiveAdmin.register_page 'Expired Subscriptions' do
  menu priority: 2, label: proc { 'Expired Subscriptions' }

  content title: proc { 'Expired Subscriptions' } do

    columns do

      column do
        panel "Подписки, истекшие неделю назад" do
          table_for EmailSubscription.where("expires < ?", 1.week.ago).includes(:user).order(expires: :desc).paginate(page: params[:page], per_page: 50) do
            column("Истекает"){ |email_subscription| email_subscription.expires }
            column("Пользователь"){ |email_subscription| link_to email_subscription.user.name, admin_user_path(email_subscription.user) if email_subscription.user }
            column("Телефон"){ |email_subscription| email_subscription.user.phone if email_subscription.user }
            column("Email"){ |email_subscription| email_subscription.user.email if email_subscription.user }
            column("Бонусные кредиты"){ |email_subscription| email_subscription.user.silver_credits if email_subscription.user }
            column("Оплаченные кредиты"){ |email_subscription| email_subscription.user.gold_credits if email_subscription.user }
          end
        end
      end
    end

    # Here is an example of a simple dashboard with columns and panels.
    #
    # columns do
    #   column do
    #     panel "Recent Posts" do
    #       ul do
    #         Post.recent(5).map do |post|
    #           li link_to(post.title, admin_post_path(post))
    #         end
    #       end
    #     end
    #   end

    #   column do
    #     panel "Info" do
    #       para "Welcome to ActiveAdmin."
    #     end
    #   end
    # end
  end # content
end
