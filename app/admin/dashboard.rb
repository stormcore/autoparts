ActiveAdmin.register_page 'Dashboard' do
  menu priority: 1, label: proc { I18n.t('active_admin.dashboard') }

  content title: proc { I18n.t('active_admin.dashboard') } do

    columns do

      column do
        panel "Подписки, истекающие ближайшую неделю" do
          table_for EmailSubscription.where("expires < ? and expires > ?", 1.week.from_now, 1.week.ago).includes(:user).order(expires: :desc).paginate(page: params[:page], per_page: 50) do
            column("Истекает"){ |email_subscription| email_subscription.expires }
            column("Пользователь"){ |email_subscription| link_to email_subscription.user.name, admin_user_path(email_subscription.user) }
            column("Телефон"){ |email_subscription| email_subscription.user.phone }
            column("Email"){ |email_subscription| email_subscription.user.email }
            column("Бонусные кредиты"){ |email_subscription| email_subscription.user.silver_credits }
            column("Оплаченные кредиты"){ |email_subscription| email_subscription.user.gold_credits }
          end
        end
      end
    end

    # Here is an example of a simple dashboard with columns and panels.
    #
    # columns do
    #   column do
    #     panel "Recent Posts" do
    #       ul do
    #         Post.recent(5).map do |post|
    #           li link_to(post.title, admin_post_path(post))
    #         end
    #       end
    #     end
    #   end

    #   column do
    #     panel "Info" do
    #       para "Welcome to ActiveAdmin."
    #     end
    #   end
    # end
  end # content
end
