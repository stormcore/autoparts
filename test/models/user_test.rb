# == Schema Information
#
# Table name: users
#
#  id                     :integer          not null, primary key
#  email                  :string           default(""), not null
#  encrypted_password     :string           default(""), not null
#  reset_password_token   :string
#  reset_password_sent_at :datetime
#  remember_created_at    :datetime
#  sign_in_count          :integer          default(0), not null
#  current_sign_in_at     :datetime
#  last_sign_in_at        :datetime
#  current_sign_in_ip     :string
#  last_sign_in_ip        :string
#  created_at             :datetime
#  updated_at             :datetime
#  name                   :string
#  phone                  :string
#  information            :string
#  city_id                :integer
#  filter_options         :hstore
#  approved               :boolean          default(FALSE), not null
#  deleted_at             :datetime
#  promo_code             :string
#  referral_code          :string
#  bonus_percentage       :float            default(10.0)
#  silver_credits         :float            default(0.0)
#  gold_credits           :float            default(0.0)
#

require 'test_helper'

class UserTest < ActiveSupport::TestCase
  # test "the truth" do
  #   assert true
  # end
end
