require 'test_helper'

class EmailSubscriptionsControllerTest < ActionController::TestCase
  setup do
    @email_subscription = email_subscriptions(:one)
  end

  test 'should get index' do
    get :index
    assert_response :success
    assert_not_nil assigns(:email_subscriptions)
  end

  test 'should get new' do
    get :new
    assert_response :success
  end

  test 'should create email_subscription' do
    assert_difference('EmailSubscription.count') do
      post :create, email_subscription: { active: @email_subscription.active, expires: @email_subscription.expires, user_id: @email_subscription.user_id }
    end

    assert_redirected_to email_subscription_path(assigns(:email_subscription))
  end

  test 'should show email_subscription' do
    get :show, id: @email_subscription
    assert_response :success
  end

  test 'should get edit' do
    get :edit, id: @email_subscription
    assert_response :success
  end

  test 'should update email_subscription' do
    patch :update, id: @email_subscription, email_subscription: { active: @email_subscription.active, expires: @email_subscription.expires, user_id: @email_subscription.user_id }
    assert_redirected_to email_subscription_path(assigns(:email_subscription))
  end

  test 'should destroy email_subscription' do
    assert_difference('EmailSubscription.count', -1) do
      delete :destroy, id: @email_subscription
    end

    assert_redirected_to email_subscriptions_path
  end
end
